# js提示弹窗

使用样例
```
layer.confirm('<div style="text-align:center"><p> 确定要删除 ID= '+id+'这个公告吗？</p></div>', {
            btn : ['确定', '取消'],
            area : ['350px' , '220px'], //设置宽高
            title : '提示'
        }, function(){//btn yes时调用的方法
            location.href = $url; 
        }, function(){ //btn calcel时调用的方法
            return false; 
        });
```